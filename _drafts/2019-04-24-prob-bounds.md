# 概率论中几个重要的不等式

本文介绍了概率论中重要的三个不等式：<u>Markov 不等式</u>、<u>Chebyshev 不等式</u>和 <u>Chernoff-Hoeffding 不等式</u>。其中 Chernoff-Hoeffding 不等式在机器学习有着广泛的应用。

## Markov 不等式

**定理（Markov 不等式）**	若随机变量 $X$ 非负，对任意常数 $a>0$，有
$$
P(X\ge a)\le\frac{\mathbb{E}[X]}{a}
$$
**证明**：
$$
\begin{align}
aP(X\ge a)&=a\int_a^\infty p(x)dx\\
&=\int_a^\infty ap(x)dx\\
&\le\int_a^\infty xp(x)dx\\
&\le\int_{-\infty}^\infty xp(x)dx\\
&=\mathbb{E}[X]
\end{align}
$$
即
$$
P(X\ge a)\le\frac{\mathbb{E}[X]}{a} \tag{Q.E.D.}
$$

## Chebyshev 不等式

**定理（Chebyshev 不等式）**	设随机变量 $X$ 的数学期望和方差都存在，则的任意常数 $\varepsilon>0$，有
$$
P(\left\vert X-\mathbb{E}[X]\right\vert\ge\varepsilon)\le\frac{ {\rm Var}(X)}{\varepsilon^2}
$$
**证明**：
$$
\begin{align}
P(\left\vert X-\mathbb{E}[X]\right\vert\ge\varepsilon)&=P((X-\mathbb{E}[X])^2\ge\varepsilon^2)\\
&\le\frac{\mathbb{E}[(X-\mathbb{E}[X])^2]}{\varepsilon^2} \tag{Markov Inequality}\\
&=\frac{ {\rm Var}(X)}{\varepsilon^2}\tag{Q.E.D.}
\end{align}
$$
