---
layout: page
title: 关于我
permalink: /about/
prefix: <i class="fas fa-user"></i>
---

**SHUAI.GURU**

喜欢的技术领域包括但不限于 Linux、C++、Docker、CUDA、Python、Web 开发；字体强迫症；略懂人工智能与机器学习，目前主要研究方向是**机器学习系统**（Systems for ML）。

最喜欢的**音乐艺人**有 Kanye West、Kendrick Lamar、Drake、J. Cole、Travis Scott、Lil Wayne、Dr. Dre、Eminem、Jay-Z 等。

最喜欢的**非技术类书籍**有《黑客与画家》、《未来简史》、《人类简史》、《三体》系列、《一个人的朝圣》、《2001：太空漫游》、《动物农场》等。

最喜欢的**电影**：《星际穿越》、《蝙蝠侠黑暗骑士》三部曲、《银翼杀手 2049》。

# 友情链接

- [PINK_PEACH_STAR](https://blog.csdn.net/PINK_PEACH_STAR)
- [Sun's Blog](https://www.virgilsun.com/)
- [Shall We Code?](https://waynerv.com/)

# 关于本站

本站使用 [Jekyll](https://jekyllrb.com/) 构建：
- Ubuntu 20.04.4
- Ruby 2.7.0
- Jekyll 4.2.1

托管于[华为云](https://www.huaweicloud.com/intl/zh-cn/)上海节点。

<script src="https://giscus.app/client.js"
        data-repo="ZJUGuoShuai/blog-comments"
        data-repo-id="R_kgDOH0RpnQ"
        data-category="Announcements"
        data-category-id="DIC_kwDOH0Rpnc4CQ0Qm"
        data-mapping="title"
        data-strict="0"
        data-reactions-enabled="1"
        data-emit-metadata="0"
        data-input-position="top"
        data-theme="light"
        data-lang="zh-CN"
        data-loading="lazy"
        crossorigin="anonymous"
        async>
</script>